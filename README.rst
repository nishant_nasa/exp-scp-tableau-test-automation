Please check the below confluence page URL for better instruction about project instruction.

http://confluence.int.corp.sun/confluence/display/CDTO/Tableau+Test+Automation+Project+Setup


1. Install Python in system:
-----------------------------
    a. Download the python version 3.5.5 for windows and follow the instruction below.
        https://www.python.org/downloads/
    b. Open the installer exe file and install it. You can refer the below URL for more information.
        https://www.ics.uci.edu/~pattis/common/handouts/pythoneclipsejava/python.html
    c. Make sure the python path is set in windows PATH environment variable,
    so that we can access the python in command line from any directory, please follow the below website for reference.
        https://www.howtogeek.com/197947/how-to-install-python-on-windows/

2. Install git:
---------------
    a. Install git by downloading the exe installer file (32 bits or 64 bits depends on your system) from below URL.
        https://git-scm.com/download/win

3. Virtual Env:
---------------

Using virtual environment (Assumes python3 is already installed in your machine)

    a. create virtual env and activate in windows
        C:\> python3 -m venv C:\Users\<your_u_number>\workspace\
        C:\> C:\Users\<your_u_number>\workspace\venv\Scripts\activate

        This below command prompt ensures the virtual environment is activated
        (venv) C:\>


    b. Download the project:
        Clone the test automation python project.
        (venv)C:\> cd C:\Users\<your_u_number>\workspace
        (venv)C:\Users\<your_u_number>\workspace\> git clone ssh://git@stash:2222/bi/utilities.git
        (venv)C:\Users\<your_u_number>\workspace\> cd utilities\tableau_test_automation

    c. Find the requirements.txt file that has all the libraries that are required. To install them, simply type.
      (venv)C:\Users\<your_u_number>\workspace\utilities\tableau_test_automation> pip install -r requirements.txt

    d. Unzip the Chrome driver file "chromedriver_win32.zip" and keep the .exe file "chromedriver.exe"
    under project directory path ().
        Step 1: Download the file "chromedriver_win32.zip" from Nexus repository
        Step 2: https://nexus/service/local/repositories/bi-api/content/selenium_chromedriver/chromedriver_win32/1/chromedriver_win32-1.zip


    e. Map the shared network drive by following below steps
        Step1: Right click "Start > Computer > Map network drive"
        Step2: In the PopUp window, map the drive ex: (L: or Z:) to this shared network drive location "\\int\groupdata\ChiefDataOffice\8. Operations Analytics\Claims Analytics - P&SC"
        Step3: mention the drive name ex: "L:" in the variable OUTPUT_DIRECTORY in constants.py file

    e. Make sure the "testcase_config\testcase_config.xlsx" is also present in the project directory and testcase details are correct in the file.

    f. Run the below command.
         (venv)C:\Users\<your_u_number>\workspace\utilities\tableau_test_automation> python run_tableau_testcases.py

    e. Once the script is completed, we can see the test report summary in the below directory path based on the value that we set
        in the variable OUTPUT_DIRECTORY in constants.py file.
        For ex: L:\tableau_test_automation\testcase_output\

    f. We can open the HTML report under the testcase_output directory that will shows the testcase run summary report (Pass/Fail).
        For ex:  L:\tableau_test_automation\testcase_output\20180417_102918_crosstab\test_report_20180417_102918_crosstab.html
        For ex:  L:\tableau_test_automation\testcase_output\20180417_103356_image\test_report_20180417_102918_crosstab.html