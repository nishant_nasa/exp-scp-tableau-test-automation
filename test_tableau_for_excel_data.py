"""
Test case for testing tableau dashboard, this test cases are used to test tableau data testing (Excel file)

"""
import datetime
import os
import shutil
import time

import chardet
import pandas as pd
from pandas.util.testing import assert_frame_equal

from constants import *
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from utility import element_has_css_class, wait_for_display, get_latest_file


class TestTableauExcelData:
    """
    Test Tableau Dashboard by downloading the data file (excel file) and match against source and target dashboards.
    """
    TIMEOUT_PERIOD = 10  # seconds

    def tableau_load(self, url, env, filter_values=None):
        """
        Loads the tableau URL and downloads the file and return the latest downloaded file location

        :param url: tableau URL, it can be source or target tableau URL
        :return: str: return latest downloaded file path
        """

        logger.info("Loading %s URL : %s ", env, url)
        driver = webdriver.Chrome()
        try:
            driver.implicitly_wait(2 * self.TIMEOUT_PERIOD)
            driver.get(url)
            time.sleep(6 * self.TIMEOUT_PERIOD)

            iframe_list = driver.find_elements_by_xpath(IFRAME_XPATH)
            # get into required iframe
            if iframe_list:
                driver.switch_to.frame(iframe_list[0])
            logger.info("Entered the iframe: %s", iframe_list)
            time.sleep(self.TIMEOUT_PERIOD)

            # if we have filter values, then choose and wait for chart to load
            if not filter_values.empty:
                for filter_column_name, filter_column_value in filter_values.iteritems():
                    filter_column_xpath = FILTER_NAME_XPATH.format(filter_column_name.strip())
                    filter_column_tag = driver.find_element_by_xpath(filter_column_xpath)
                    filter_column_tag.click()
                    logger.info("Clicked filter: (%s)", filter_column_name)
                    time.sleep(self.TIMEOUT_PERIOD)
                    # choose the menu item
                    menu_item = FILTER_VALUE_XPATH.format(filter_column_value.strip())
                    menu_item_tag = driver.find_element_by_xpath(menu_item)
                    menu_item_tag.click()
                    logger.info("Clicked filter menu-item: (%s)", filter_column_value)
                    time.sleep(3 * self.TIMEOUT_PERIOD)

            # this click to make sure UI makes an Ajax call, so that Crosstab menu item will be enabled to click
            div_tag = driver.find_element_by_xpath(DIV_DISPLAY_CSS_XPATH)
            div_tag.click()
            logger.info("Clicked div_tag for ajax call: %s" % div_tag)
            time.sleep(self.TIMEOUT_PERIOD)

            a_tag = driver.find_element_by_xpath(A_TAG_EXPORT_XPATH)
            a_tag.click()
            logger.info("Clicked a_tag: %s" % a_tag)
            time.sleep(self.TIMEOUT_PERIOD / 5)

            crosstab_tag = driver.find_element_by_xpath(CROSSTAB_CLICK_XPATH)
            crosstab_tag.click()
            logger.info("Clicked crosstab_tag")
            time.sleep(self.TIMEOUT_PERIOD)

            download_tag = driver.find_element_by_xpath(DOWNLOAD_CLICK_XPATH)
            download_tag.click()
            logger.info("Clicked download_tag")

            # wait until the file is downloaded using sleep method as we don't have inbuilt method to check.
            time.sleep(3 * self.TIMEOUT_PERIOD)
            # get the latest downloaded file
            if download_tag:
                latest_file = get_latest_file()
                return latest_file
            else:
                raise Exception
        except:
            logger.error("File could not be downloaded for url: %s", url)
        finally:
            driver.stop_client()
            driver.close()
            driver.quit()

    def test_tableau_dashboard__excel_data_comparison(self, index, tcase_name, s_url, t_url, all_row_data, output_dir):
        """
        Test case for testing tableau dashboard, do assert match on excel file downloaded from source and target
        tableau dashboard URL.

        :param index: index value, passed from the test case configuration file
        :param tcase_name:  test case name, passed from test case configuration file
        :param s_url: source URL, passed from test case configuration file
        :param t_url: target URL, passed from test case configuration file
        :param output_dir: test case output directory where the downloaded files and test summary report are kept.

        :return: None
        """
        logger.info("== Start of tableau test case == ")
        # drop filter column if they have nan/null value
        temp_filter_values = all_row_data[4:].dropna()

        if not temp_filter_values.empty:
            index_str = index + "_" + "filter"
        else:
            index_str = index

        dir_name = index_str + "_" + tcase_name
        tc_output_dir = os.path.join(output_dir, dir_name)

        if not os.path.exists(tc_output_dir):
            os.makedirs(tc_output_dir)

        # source file
        source_downloaded_file = self.tableau_load(url=s_url, env="source", filter_values=temp_filter_values)
        source_file_name = index_str + "_" + "source_" + os.path.basename(source_downloaded_file)
        source_output_file_path = os.path.join(tc_output_dir, source_file_name)
        shutil.move(source_downloaded_file, source_output_file_path)

        # target file
        target_download_file = self.tableau_load(url=t_url, env="target", filter_values=temp_filter_values)
        target_file_name = index_str + "_" + "target_" + os.path.basename(target_download_file)
        target_output_file_path = os.path.join(tc_output_dir, target_file_name)
        shutil.move(target_download_file, target_output_file_path)

        # get the encode method used
        with open(source_output_file_path, 'rb') as f_obj:
            source_result = chardet.detect(f_obj.read())
        # get the encode method used
        with open(target_output_file_path, 'rb') as f_obj:
            target_result = chardet.detect(f_obj.read())

        df_source = pd.read_csv(source_output_file_path,
                                encoding=source_result['encoding'], delim_whitespace=True)

        df_target = pd.read_csv(target_output_file_path,
                                encoding=target_result['encoding'], delim_whitespace=True)
        # assert the two downloaded files
        # assert_frame_equal(df_source.reset_index(drop=True), df_target.reset_index(drop=True))
        assert_frame_equal(df_source, df_target)

