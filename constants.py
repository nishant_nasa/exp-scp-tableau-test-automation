import logging

# Default chrome browser download location
DOWNLOAD_LOCATION = "H:\\Downloads\\*"

# copy the downloaded file and test report summary file to shared network directory path,
# to do that we have follow the below steps
# Step1: Right click "Start > Computer > Map network drive"
# Step2: In the PopUp window, map the drive ex: (L: or Z:) to this shared network drive
# location "\\int\groupdata\ChiefDataOffice\8. Operations Analytics\Claims Analytics - P&SC"
# Step3: mention the drive name ex: "L:" in the below constant variable OUTPUT_DIRECTORY.
OUTPUT_DIRECTORY = "C:\\Users\\u205164\\Documents\\tableau_tester"

# change the location based on your project path
TESTCASE_CONFIG_FILE_PATH = "C:\\Users\\u205164\\projects\\repos\\utilities\\tableau_test_automation\\testcase_config\\" \
                            "testcase_config_filter_group_and_individual.xlsx"

LIST_OF_TESTCASES = [
    # {'crosstab': "test_tableau_for_excel_data.py"},
    {'image': "test_tableau_image.py"},
]

# Common XPATH
IFRAME_XPATH = "//iframe[@allowtransparency='true']"
# LOADING_SPINNER_XPATH = '//*[@id="loadingSpinner"]'
A_TAG_EXPORT_XPATH = "//a[@title='Export']"
DOWNLOAD_CLICK_XPATH = "//span[@class='tab-styledButtonMiddle' and text()='Download']"

# Crosstab/Excel sheet download XPATH details
DIV_DISPLAY_CSS_XPATH = "//div[contains(@class, 'tab-dashboard')]"
CROSSTAB_XPATH = "//span[text()='Crosstab']/../.."
CROSSTAB_CSS = "tabMenuItem tabMenuItemBaseTheme tabMenuMenuItem"
CROSSTAB_CLICK_XPATH = "//span[text()='Crosstab']"

# Image download XPATH
IMAGE_CLICK_XPATH = "//span[text()='Image']"

CMP_RESULT_IMAGE_NAME = "compared_result.png"
CMP_RESULT_PDF_NAME = "compared_result.pdf"

# Log settings
logger = logging.getLogger('tableau_automated_tester')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler("tableau_automated_tester.log")
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)

# filter column XPATH
FILTER_NAME_XPATH = "//span[text()=' {0}']/../../../../..//div[@class='tabComboBoxNameContainer tab-ctrl-formatted-fixedsize']"
FILTER_VALUE_XPATH = "//div[@class='tabMenuContent']/descendant::span[text()='{0}']"
