"""
Test case for testing tableau dashboard, this test cases are used to test tableau data testing (Excel file)

"""
import datetime

import os
import shutil
import time

import cv2
import matplotlib.pyplot as plt
import numpy as np
from skimage.io import imread, imsave
from skimage.measure import compare_ssim

from constants import *
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from utility import element_has_css_class, wait_for_display, get_latest_file


class TestTableauImageData:
    """
    Test Tableau Dashboard by downloading the data file (excel file) and match against source and target dashboards.
    """

    TIMEOUT_PERIOD = 10  # seconds

    def tableau_load(self, url, env, filter_values=None):
        """
        Loads the tableau URL and downloads the file and return the latest downloaded file location

        :param url: tableau URL, it can be source or target tableau URL
        :return: str: return latest downloaded file path
        """

        logger.info("Loading %s URL : %s ", env, url)
        driver = webdriver.Chrome()
        try:
            driver.implicitly_wait(2 * self.TIMEOUT_PERIOD)
            driver.get(url)
            time.sleep(6 * self.TIMEOUT_PERIOD)

            iframe_list = driver.find_elements_by_xpath(IFRAME_XPATH)
            # get into required iframe
            if iframe_list:
                driver.switch_to.frame(iframe_list[0])
            logger.info("Entered the iframe: %s", iframe_list)
            time.sleep(self.TIMEOUT_PERIOD)

            # if we have filter values, then choose and wait for chart to load
            if not filter_values.empty:
                for filter_column_name, filter_column_value in filter_values.iteritems():
                    filter_column_xpath = FILTER_NAME_XPATH.format(filter_column_name.strip())
                    filter_column_tag = driver.find_element_by_xpath(filter_column_xpath)
                    filter_column_tag.click()
                    logger.info("Clicked filter: (%s)", filter_column_name)
                    time.sleep(self.TIMEOUT_PERIOD)
                    # choose the menu item
                    menu_item = FILTER_VALUE_XPATH.format(filter_column_value.strip())
                    menu_item_tag = driver.find_element_by_xpath(menu_item)
                    menu_item_tag.click()
                    logger.info("Clicked filter menu-item: (%s)", filter_column_value)
                    time.sleep(3 * self.TIMEOUT_PERIOD)

            a_tag = driver.find_element_by_xpath(A_TAG_EXPORT_XPATH)
            a_tag.click()
            logger.info("Clicked a_tag")
            time.sleep(self.TIMEOUT_PERIOD/5)

            image_tag = driver.find_element_by_xpath(IMAGE_CLICK_XPATH)
            image_tag.click()
            logger.info("Clicked image_tag")
            time.sleep(self.TIMEOUT_PERIOD)

            download_tag = driver.find_element_by_xpath(DOWNLOAD_CLICK_XPATH)
            download_tag.click()
            logger.info("Clicked download_tag")

            # wait until the file is downloaded using sleep method as we don't have inbuilt method to check.
            time.sleep(self.TIMEOUT_PERIOD)
            # get the latest downloaded file
            if download_tag:
                latest_file = get_latest_file()
                return latest_file
            else:
                raise Exception
        except:
            logger.error("File could not be downloaded for url: %s", url)
        finally:
            driver.stop_client()
            driver.close()
            driver.quit()

    def test_tableau_dashboard__image_comparison(self, index, tcase_name, s_url, t_url, all_row_data, output_dir):
        """
        Test case for testing tableau dashboard, do comparison on image file downloaded from source and target
        tableau dashboard URL. And it generates a PDF file
+
        :param index: index value, passed from the test case configuration file
        :param tcase_name:  test case name, passed from test case configuration file
        :param s_url: source URL, passed from test case configuration file
        :param t_url: target URL, passed from test case configuration file
        :param output_dir: test case output directory where the downloaded files and test summary report are kept.

        :return: None
        """
        logger.info("== Start of tableau test case == ")
        # drop filter column if they have nan/null value
        temp_filter_values = all_row_data[4:].dropna()

        if not temp_filter_values.empty:
            index_str = index + "_" + "filter"
        else:
            index_str = index

        dir_name = index_str + "_" + tcase_name
        tc_output_dir = os.path.join(output_dir, dir_name)

        if not os.path.exists(tc_output_dir):
            os.makedirs(tc_output_dir)

        # source file
        source_downloaded_file = self.tableau_load(url=s_url, env="source", filter_values=temp_filter_values)
        source_file_name = index_str + "_" + "source_" + os.path.basename(source_downloaded_file)
        source_output_file_path = os.path.join(tc_output_dir, source_file_name)
        shutil.move(source_downloaded_file, source_output_file_path)

        # target file
        target_download_file = self.tableau_load(url=t_url, env="target", filter_values=temp_filter_values)
        target_file_name = index_str + "_" + "target_" + os.path.basename(target_download_file)
        target_output_file_path = os.path.join(tc_output_dir, target_file_name)
        shutil.move(target_download_file, target_output_file_path)

        # load the two input images
        source_image = imread(source_output_file_path)
        target_image = imread(target_output_file_path)

        source_gray = cv2.cvtColor(source_image, cv2.COLOR_BGR2GRAY)
        target_gray = cv2.cvtColor(target_image, cv2.COLOR_BGR2GRAY)

        (score, difference) = compare_ssim(source_gray, target_gray, full=True, multichannel=True)
        result = not np.any(difference)

        if score == 1.0:
            logger.info("Images are same")
            assert 1
        else:
            logger.info("Images are not same")
            cmp_result_dir_path = os.path.dirname(os.path.abspath(target_output_file_path))
            cmp_result_image_name = index_str + "_" + os.path.basename(target_download_file) + "_" + CMP_RESULT_IMAGE_NAME
            cmp_result_pds_name = index_str + "_" + os.path.basename(target_download_file) + "_" + CMP_RESULT_PDF_NAME
            cmp_result_img_file_path = os.path.join(cmp_result_dir_path, cmp_result_image_name)
            cmp_result_pdf_file_path = os.path.join(cmp_result_dir_path, cmp_result_pds_name)

            imsave(cmp_result_img_file_path, difference)
            time.sleep(self.TIMEOUT_PERIOD/3)
            result = imread(cmp_result_img_file_path)

            fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(70, 70))
            ax = axes.ravel()
            ax[0].imshow(source_image)
            ax[0].set_title('Source', fontdict={"fontsize": "16"})

            ax[1].imshow(target_image)
            ax[1].set_title('Target', fontdict={"fontsize": "16"})

            ax[2].imshow(result, cmap='Blues')
            ax[2].set_title('Difference', fontdict={"fontsize": "16"})

            ax[3].imshow(source_image, cmap='gray',  interpolation='none')
            ax[3].imshow(result, cmap='jet', alpha=0.5, interpolation='none')
            ax[3].set_title('Difference overlay type 1 on target image', fontdict={"fontsize": "16"})

            ax[4].imshow(target_image)
            ax[4].imshow(result, cmap='jet', alpha=0.5)
            ax[4].set_title('Difference overlay type 2 on target image', fontdict={"fontsize": "16"})

            plt.savefig(cmp_result_pdf_file_path, format='pdf')
            assert 0
