"""
This module is used for setting configuration specifically for pytest.

We use this module for dynamically pass the parameter values from test case configuration excel file to
python test case function.

"""
import pandas as pd


def pytest_addoption(parser):
    """
    Defined customized parameter values other than default pytest parameter values

    :param parser: pytest object
    :return: None
    """
    parser.addoption("--config_file", action="append", default=[])
    parser.addoption("--output_dir", action="append", default=[])


def pytest_generate_tests(metafunc):
    """
    Parse the test case configuration file and read test case input values.

    :param metafunc: pytest object which contains the command line arguments.
    :return:
    """
    # This is called for every test. Only get/set command line arguments
    # if the argument is specified in the list of test "fixturenames".
    # if 'config_file' in metafunc.fixturenames:
    all_list_of_data = tuple()
    xls = pd.ExcelFile(metafunc.config.getoption('config_file')[0])
    df_config = pd.read_excel(xls, 'Index')
    df_execute_sheets = df_config[df_config['Execution'] == 'Yes']
    output_dir = metafunc.config.getoption('output_dir')[0]

    for index_sheet, df_row in df_execute_sheets.iterrows():
        df_sheet = pd.read_excel(xls, df_row['Sheet No'])
        sheet_no_str = "sheet_no_" + str(df_row['Sheet No'])
        list_of_data = tuple((sheet_no_str + "_" + "test_case_no_" + str(index + 1), row[1], row[2], row[3], row, output_dir)
                             for index, row in df_sheet.iterrows())
        all_list_of_data = all_list_of_data + list_of_data

    metafunc.parametrize(["index", "tcase_name", "s_url", "t_url", "all_row_data", "output_dir"], [*all_list_of_data])
