"""
Usage of this module are listed below.

1. Iterate over the python test files from constant dict variable LIST_OF_TESTCASES
2. Create output directory
3. Call pytest command for each python test file with required parameter like testcase_config file path,
output directory path, etc.

"""
import datetime
import os

import pytest
from constants import TESTCASE_CONFIG_FILE_PATH, LIST_OF_TESTCASES, OUTPUT_DIRECTORY


class MyPlugin(object):
    def pytest_sessionfinish(self):
        print("*** test run reporting finishing")


def run_main():
    # create test case report output directory
    for tc_details in LIST_OF_TESTCASES:
        for testcase_name, testcase_file_name in tc_details.items():
            datetime_dir = datetime.datetime.strftime(datetime.datetime.now(), "%Y%m%d_%H%M%S") + "_" + testcase_name
            html_file_name = "test_report_" + datetime_dir + ".html"
            output_dir_path = os.path.join(OUTPUT_DIRECTORY, datetime_dir)

            if not os.path.exists(output_dir_path):
                os.makedirs(output_dir_path)
                output_file_path_data = os.path.join(output_dir_path, html_file_name)

            pytest.main(["-s", testcase_file_name,
                         "--config_file", TESTCASE_CONFIG_FILE_PATH, "--html",
                         output_file_path_data, "--output_dir", output_dir_path],
                        plugins=[MyPlugin()])


if __name__ == "__main__":
    run_main()
